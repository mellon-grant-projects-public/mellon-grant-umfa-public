<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    exclude-result-prefixes=""
    version="1.0">
    <xsl:template match="sydney/import/template">
        <records>
            <xsl:for-each select="record">
        <oai_qdc:qualifieddc xmlns:oai_qdc="http://worldcat.org/xmlschemas/qdc-1.0/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://worldcat.org/xmlschemas/qdc-1.0/ http://worldcat.org/xmlschemas/qdc/1.0/qdc-1.0.xsd http://purl.org/net/oclcterms http://worldcat.org/xmlschemas/oclcterms/1.4/oclcterms-1.4.xsd">
            <dc:title>
                <xsl:value-of select="field[@id='Title']">
                </xsl:value-of>
            </dc:title>
            <dc:creator>
                <xsl:value-of select="link[@id='ArtistLCN1']">
                </xsl:value-of>
            </dc:creator>
            <dc:description>
                <xsl:value-of select="field[@id='ObjDescr']">
                </xsl:value-of>
            </dc:description>
              <xsl:for-each select="template[@id='NamedDates']/record/field[@id='DateRange']">
                      <dc:date>
                          <xsl:apply-templates select="text()"></xsl:apply-templates>
                      </dc:date>
              </xsl:for-each>


            <dc:publisher></dc:publisher>
            <dc:type></dc:type>
            <dc:format></dc:format>
            <dc:rights></dc:rights>
            <dc:identifier><xsl:value-of select="field[@id='ObjectID']"></xsl:value-of></dc:identifier>

            <xsl:for-each select="template[@id='Label']/record/field[@id='Label']">
                <dcterms:abstract>
                    <xsl:apply-templates select="text()"></xsl:apply-templates>
                </dcterms:abstract>
            </xsl:for-each>

            <xsl:for-each select="link[@id='MatTechAA1']">
                <dcterms:medium>
                    <xsl:apply-templates select="text()"></xsl:apply-templates>
                </dcterms:medium>
            </xsl:for-each>
            <dcterms:hasFormat><xsl:value-of select="link[@id='ObjectName']">
            </xsl:value-of></dcterms:hasFormat>
            <dcterms:temporal><xsl:value-of select="link[@id='ObjOrigin']"></xsl:value-of></dcterms:temporal>
            <dcterms:abstract><xsl:value-of select="field[@id='PublicDesc']"></xsl:value-of></dcterms:abstract>
            <dcterms:educationLevel><xsl:value-of select="field[@id='SystemKey']"></xsl:value-of></dcterms:educationLevel>
            <xsl:for-each select="template[@id='Image']/record/field[@id='SystemKey']">
                    <dcterms:mediator>
                        <xsl:apply-templates select="text()"></xsl:apply-templates>
                    </dcterms:mediator>
            </xsl:for-each>

        </oai_qdc:qualifieddc>

        </xsl:for-each>
        </records>
    </xsl:template>
    </xsl:stylesheet>
