<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    exclude-result-prefixes=""
    version="1.0">
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="records">

                    <OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
                        <responseDate>2019-07-10T16:38:10Z</responseDate>
                        <request verb="ListRecords" metadataPrefix="oai_qdc" set="argus">http://uvu.contentdm.oclc.org/oai/oai.php</request>
                        <ListRecords>
                            <xsl:for-each select="node()|@*">
                               <record>
                                <header>
                                    <identifier>oai:uvu.contentdm.oclc.org:argus/<xsl:value-of select="position()"></xsl:value-of></identifier>
                                    <datestamp>2018-06-03</datestamp>
                                    <setSpec>argus</setSpec>
                                </header>
                                <metadata>
                                   <xsl:copy-of select="self::node()" ></xsl:copy-of>
                                </metadata>
                               </record>
                            </xsl:for-each>
                        </ListRecords>
                    </OAI-PMH>

    </xsl:template>


</xsl:stylesheet>
