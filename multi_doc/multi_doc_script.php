<?php
$files = array();
foreach(scandir("files/") as $file){
  if(strpos($file,".xml") != false){
    array_push($files, $file);
  }
};

if(!file_exists("output")){
  mkdir("output");
}

if(!file_exists("finished")){
  mkdir("finished");
}

//Hash Code File
if(!file_exists("hash")){
  mkdir("hash");
}
$hash_files = array();
foreach(scandir("hash/") as $hash_file){
  if(strpos($hash_file,".csv") != false){
    array_push($hash_files, $hash_file);
  }
};

//1. Open csv file provided, store it as dictionary
$hash_file = $hash_files[0];

$csv = array();
$lines = file('hash/'.$hash_file, FILE_IGNORE_NEW_LINES);

foreach ($lines as $key => $value)
{
    if(empty(str_getcsv($value)[0]))
      break;

    $syskey = str_getcsv($value)[0];
    $hash = substr(str_getcsv($value)[1],2);

    //$csv[$key] = array("syskey"=> trim($syskey), "hash"=> trim($hash));
    $csv[strtolower(trim($syskey))] = strtolower(trim($hash));

}

//Handle XML File Process
foreach($files as $file){
  //process xml
  $file_name = $file;

  echo "Now processing ". $file_name."\n";

  // Load original XML file
  $xml = new DOMDocument;
  $xml->load("files/".$file_name);

  // Load XSL file to parse CDATA
  $xsl = new DOMDocument;
  $xsl->load('parse_cdata.xsl');

  // Configure the transformer
  $proc = new XSLTProcessor;

  // Attach the xsl rules
  $proc->importStyleSheet($xsl);

  $parsed_xml = $proc->transformToXML($xml);

  // Extract OAI from document
  $xml = new DOMDocument;
  $xml->load("files/".$file_name);

  $xsl->load('oai_parse_multi.xsl');
  // Configure the transformer
  $proc = new XSLTProcessor;

  // Attach the xsl rules
  $proc->importStyleSheet($xsl);

  $parsed_xml = $proc->transformToXML($xml);

  file_put_contents("output/".pathinfo($file_name, PATHINFO_FILENAME) . "_converted.xml", $parsed_xml);

  // Format Transformation
  $xml = new DOMDocument;
  $xml->load("output/".pathinfo($file_name, PATHINFO_FILENAME). "_converted.xml");

  $xsl->load('transform_format_multi.xsl');

  // Attach the transform rules
  $proc->importStyleSheet($xsl);

  $new_xml =  $proc->transformToXML($xml);

  file_put_contents("output/".pathinfo($file_name, PATHINFO_FILENAME). "_converted.xml", $new_xml);


  // Cont. Format Transformation
  $xml = new DOMDocument;
  $xml->load("output/".pathinfo($file_name, PATHINFO_FILENAME). "_converted.xml");

  $records = $xml->getElementsByTagName('qualifieddc');
  foreach ($records as $record) {
       // insert xmlns:xsi attribute
       $record->setAttribute( "xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance" );
       // insert current date

  }

  //Load Hash File
  $records = $xml->getElementsByTagName('mediator');

  $image_syskeys_missing = array();
  $nodes_to_delete = array();
  $image_syskeys = array();



  foreach ($records as $record) {
       // insert xmlns:xsi attribute
       //$record->setAttribute( "xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance" );
       // insert current date
       //echo $record->textContent . "\n";
       //3. Look up hascode and remove first 2 chars in the code and replace system key part
       if(!empty($csv[trim($record->textContent)])){
         array_push($image_syskeys, trim($record->textContent));
         //echo trim($record->textContent). ": ".$csv[trim($record->textContent)] . "\n";
         $record->nodeValue = $csv[trim($record->textContent)];
       }
       else{
         //mark it as not found
         //delete the node
         //Count how many missing for syskey not found in CSV file
         array_push($image_syskeys_missing, trim($record->textContent));
         array_push($nodes_to_delete, $record);
         //$record->nodeValue = $csv[trim($record->textContent)];
         //$record->parentNode->removeChild($record);

       }
  }

  for($i =0; $i< count($nodes_to_delete); $i++){
      $nodes_to_delete[$i]->parentNode->removeChild($nodes_to_delete[$i]);
  }

  //echo "In total ".count($image_syskeys). " records records found in CSV files.\n";
  //echo "In total ".count($image_syskeys_missing). " records missing records in CSV files.\n";

  //4. Save XML object back to file
  $result = $xml->saveXML();
  file_put_contents("output/".pathinfo($file_name, PATHINFO_FILENAME). "_converted.xml", $result);


  rename("files/".$file_name, "finished/".$file_name);
}

?>
