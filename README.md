**What it does**

Format XML file for Primo Harvest using XSL file.

**Installations**
1. make sure you have php installed abd enable "extension=php_xsl.dll" or "extension=xsl.dll" in php.ini
2. Set up Environment for php command
3. checkout code from repo by running "git clone git@gitlab.lib.utah.edu:mellon-grant-projects-public/mellon-grant-umfa-public.git your_project_name"


**Steps**
1. Place/upload files to "root_to_project/multi_doc/files/"
2. Download the latest hash files to "root_to_project/multi_doc/hash/" and convert file to "csv" format
3. Open Command Prompt, navigate and run script by running "cd root_to_project/multi_doc/" and "php multi_doc_script.php", output file in xml format is under "output" folder
4. Compress and transform uploaded xml file as  '.tar.gz' (you can do it by running "tar -czvf name-of-archive.tar.gz /path/to/directory-or-file tar -czvf output_file_name.tar.gz path_to_file")
5. The '.tar.gz' file is ready for harvest process
6. Upload files to server under 'primo' folder




**Resources**

1. PHP install on windows: https://www.wampserver.com/en/
